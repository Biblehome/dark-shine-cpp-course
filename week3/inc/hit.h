#ifndef HIT_H
#define HIT_H

#include "TObject.h"

enum DetectorType{cUnkown, cTracker, cECal, cHCal};

class Hit : public TObject
{
    ClassDef(Hit, 1);

public:
    Hit();
    Hit(const Hit &old_hit);
    virtual ~Hit();

    void SetId(int new_id) {id_ = new_id;}
    void SetDetectorType(DetectorType new_detector) {detector_ = new_detector;}
    void SetX(double new_x) {x_ = new_x;}
    void SetY(double new_y) {y_ = new_y;}
    void SetZ(double new_z) {z_ = new_z;}
    void SetTime(double new_time) {time_ = new_time;}
    void SetEnergy(double new_energy) {energy_ = new_energy;}

    int GetId() const {return id_;}
    DetectorType GetDetectorType() const {return detector_;}
    double GetX() const {return x_;}
    double GetY() const {return y_;}
    double GetZ() const {return z_;}
    double GetTime() const {return time_;}
    double GetEnergy() const {return energy_;}
    virtual void Print(Option_t *option="") const;

    virtual Int_t Compare(const TObject *obj) const;
    bool operator <(const Hit &new_hit) const;
    bool operator ==(const Hit &new_hit) const;
    Hit& operator =(const Hit &new_hit);

    virtual TObject* FindObject(const char *name) const {return nullptr;}
    virtual TObject* FindObject(const TObject *obj) const {return nullptr;}

private:
    int id_{0};

    DetectorType detector_{cUnkown};

    double x_{0.};
    double y_{0.};
    double z_{0.};
    double time_{0.};
    double energy_{0.};
};

#endif
