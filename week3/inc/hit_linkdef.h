#ifndef HIT_LINK_DEF_H
#define HIT_LINK_DEF_H

#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ class Hit+;

#endif

#endif

