#include "hit.h"

#include <iostream>

#include "TMessage.h"
#include "TError.h"

Hit::Hit() : id_(0),
             x_(0.),
             y_(0.),
             z_(0.),
             time_(0.),
             energy_(0.)
{
}

Hit::Hit(const Hit &old_hit) : TObject(old_hit),
                               id_(old_hit.id_),
                               x_(old_hit.x_),
                               y_(old_hit.y_),
                               z_(old_hit.z_),
                               time_(old_hit.time_),
                               energy_(old_hit.energy_)
{
}

Hit::~Hit()
{
}

void Hit::Print(Option_t* ) const
{
    std::cout << "************************************************************" << std::endl
              << "* Hit                                                      *" << std::endl
              << "************************************************************" << std::endl
              << "* id         : " << id_ << std::endl
              << "* x/mm       : " << x_  << std::endl
              << "* y/mm       : " << y_  << std::endl
              << "* z/mm       : " << z_  << std::endl
              << "* time/ns    : " << time_ << std::endl
              << "* energy/MeV : " << energy_ << std::endl
              << "************************************************************" << std::endl;
}

Int_t Hit::Compare(const TObject *obj) const
{
    const Hit *new_hit = dynamic_cast<const Hit*>(obj);
    if(!new_hit)
    {
        Error("Compare", "input is not a Hit object");
        return 0;
    }

    if(id_ < new_hit->id_) return -1;
    if(id_ > new_hit->id_) return  1;
    return 0;
}

bool Hit::operator <(const Hit &new_hit) const
{
    if(this->Compare(&new_hit) == -1) return true;
    return false;
}

bool Hit::operator ==(const Hit &new_hit) const
{
    if(id_ != new_hit.id_)             return false;
    if(detector_ != new_hit.detector_) return false;
    if(x_ != new_hit.x_)               return false;
    if(y_ != new_hit.y_)               return false;
    if(z_ != new_hit.z_)               return false;
    if(time_ != new_hit.time_)         return false;
    if(energy_ != new_hit.energy_)     return false;

    return true;
}

Hit& Hit::operator =(const Hit &new_hit)
{
    if(this == &new_hit) return *this;

    TObject::operator=(new_hit);
    id_ = new_hit.id_;
    detector_ = new_hit.detector_;
    x_ = new_hit.x_;
    y_ = new_hit.y_;
    z_ = new_hit.z_;
    time_ = new_hit.time_;
    energy_ = new_hit.energy_;

    return *this;
}
