#include <iostream>
#include <algorithm>
#include <vector>
#include <map>
#include <set>

#include "TFile.h"
#include "TTree.h"

#include "hit.h"

int main(int argc, char *argv[]) {
    Hit hit_raw;
    std::cout << "size of hit    " << sizeof(hit_raw) << std::endl;
    std::cout << "size of int    " << sizeof(0) << std::endl;
    std::cout << "size of double " << sizeof(0.) << std::endl;

    {
        std::cout << "\nROOT" << std::endl;

        auto file = new TFile("test.root", "recreate");
        auto tree = new TTree("test", "test");
        tree->Branch("hit", &hit_raw, 320000, 0);

        for (int i = 0; i < 10; ++i) {
            hit_raw.SetId(i);
            hit_raw.SetX(-i);
            tree->Fill();
            std::cout << "save hit id: " << hit_raw.GetId() << std::endl;
        }

        tree->Write();
        file->Close();
        std::cout << "save tree" << std::endl;
    }

    {
        std::cout << "\nstd::vector" << std::endl;

        std::vector<Hit> hits;
        for (int i = 0; i < 10; ++i) {
            std::cout << "hits.size : " << hits.size() << std::endl;
            std::cout << "hits.capacity : " << hits.capacity() << std::endl;
            hit_raw.SetId(i);
            hit_raw.SetX(-i);
            hits.push_back(hit_raw);
        }

        std::cout << "for(int i = 0; i < hits.size(); ++i)" << std::endl;
        for (int i = 0; i < hits.size(); ++i) std::cout << "    hit id: " << hits.at(i).GetId() << std::endl;

        std::cout << "for(std::vector<hit>::iterator it = hits.begin();" << std::endl
                  << "                               it != hits.end();" << std::endl
                  << "                             ++it)" << std::endl;
        for (std::vector<Hit>::iterator it = hits.begin();
             it != hits.end();
             ++it)
            std::cout << "    hit id: " << it->GetId() << std::endl;

        std::cout << "for(Hit hit : hits)" << std::endl;
        for (const Hit &hit: hits) std::cout << "    hit id: " << hit.GetId() << std::endl;

        std::cout << "after sort: " << std::endl;
        std::sort(hits.begin(), hits.end(),
                  [&](const Hit &hit_1, const Hit &hit_2) -> bool { return hit_1.GetId() > hit_2.GetId(); }
        );
        for (const Hit &hit: hits) std::cout << "    hit id: " << hit.GetId() << std::endl;

        std::cout << "compare hit id " << hits.at(0).GetId() << " & hit id " << hits.at(9).GetId() << ": "
                  << hits.at(0).Compare(&hits.at(9)) << std::endl;

        std::cout << "remove hit id: " << hits.at(3).GetId() << std::endl;
        hits.erase(std::remove(std::begin(hits), std::end(hits), hits.at(3)),
                   std::end(hits));
        for (const Hit &hit: hits) std::cout << "    hit id: " << hit.GetId() << std::endl;
    }

    {
        std::cout << "\nstd::set" << std::endl;

        std::set<Hit> hits;
        for (int i = 0; i < 10; ++i) {
            hit_raw.SetId(i);
            hits.insert(hit_raw);
        }
        for (const Hit &hit: hits) std::cout << "    hit id: " << hit.GetId() << std::endl;

        std::cout << "add duplicate hit id: " << hit_raw.GetId() << std::endl;
        hits.insert(hit_raw);
        for (const Hit &hit: hits) std::cout << "    hit id: " << hit.GetId() << std::endl;

        std::cout << "add new hit id: " << hit_raw.GetId() << std::endl;
        hit_raw.SetId(-1);
        hits.insert(hit_raw);
        for (const Hit &hit: hits) std::cout << "    hit id: " << hit.GetId() << std::endl;
    }

    {
        std::cout << "\nstd::map" << std::endl;

        std::map<int, Hit> hits;
        for (int i = 0; i < 10; ++i) {
            hit_raw.SetId(i);
            hits.insert(std::make_pair(hit_raw.GetId(), hit_raw));
        }

        for (const auto &hit: hits) std::cout << "    hit id: " << hit.second.GetId() << std::endl;
        std::cout << "add duplicate hit id: " << hit_raw.GetId() << std::endl;
        hits.insert(std::make_pair(hit_raw.GetId(), hit_raw));
        for (const auto &hit: hits) std::cout << "    hit id: " << hit.second.GetId() << std::endl;
        hit_raw.SetId(-1);
        hits.insert(std::make_pair(hit_raw.GetId(), hit_raw));
        std::cout << "add new hit id: " << hit_raw.GetId() << std::endl;
        for (const auto &hit: hits) std::cout << "    hit id: " << hit.second.GetId() << std::endl;
    }
}
