#include "student.h"

#include <iostream>
#include <algorithm>

Student::Student(TString name, Gender gender) : name_(name),
                                                gender_(gender),
                                                office_(nullptr)
{
    std::cout << "Sign up student information ..." << std::endl;
}

Student::Student(const Student &old_student) : name_(old_student.name_),
                                               gender_(old_student.gender_),
                                               friends_of_student_(old_student.friends_of_student_),
                                               office_(nullptr)
{
    std::cout << "Copy student information of " << old_student.GetStudentName() << " ..." << std::endl;

    if(old_student.office_) office_ = new int(*old_student.office_);
}

Student::~Student()
{
    std::cout << "Information of " << name_ << " completed, sign off ..." << std::endl;

    DeleteFriend(*this);
    delete office_; office_ = nullptr;
}

void Student::AddStudentFriend(Student &new_friend)
{
    if(this == &new_friend) return;

    for(const auto friend_of_student_ : friends_of_student_)
    {
        if(friend_of_student_ == &new_friend)
            return;
    }

    friends_of_student_.push_back(&new_friend);
    new_friend.friends_of_student_.push_back(this);
}

void Student::DeleteFriend(Student &old_friend)
{
    for(const auto friend_of_student_ : friends_of_student_)
        friend_of_student_->friends_of_student_.erase(std::remove(std::begin(friend_of_student_->friends_of_student_),
                                                                  std::end(friend_of_student_->friends_of_student_),
                                                                  this),
                                                      std::end(friend_of_student_->friends_of_student_));
}

std::ostream& operator<<(std::ostream &os, const Student &student)
{
    os << "************************" << std::endl
       << "Name of Student        : " << student.name_    << std::endl
       << "Title                  : " << "Common Student" << std::endl
       << "Gender                 : " << ((student.gender_ == 0) ? "female" : "male") << std::endl
       << "Office                 : " << ((student.office_ == nullptr) ? "" : TString::Itoa(*student.office_, 10)) << std::endl
       << "Friends of the student : ";
    for(auto friend_of_student_ = student.friends_of_student_.begin();
        friend_of_student_ != student.friends_of_student_.end();
        ++ friend_of_student_)
        os << (*friend_of_student_)->name_ 
           << ((friend_of_student_ == student.friends_of_student_.end() - 1) ? "" : ", ");
    os << std::endl
       << "************************" << std::endl;

    return os;
}
