#include "zhangyulei.h"

#include <iostream>

ZhangYulei::ZhangYulei() : Student("Zhang Yulei", Student::dMale),
                           car_number_("沪XXXXXX")
{
    std::cout << "Sign up specific student information of Zhang Yulei ..." << std::endl;
}

ZhangYulei::ZhangYulei(const ZhangYulei &old_zhangyulei) : Student(old_zhangyulei),
                                                           car_number_(old_zhangyulei.car_number_)
{
    std::cout << "Copy student information of Zhang Yulei from old database of Zhang Yulei..." << std::endl;
}

ZhangYulei::~ZhangYulei()
{
    std::cout << "Specific information of Zhang Yulei completed, sign off ..." << std::endl;
}

std::ostream& operator<<(std::ostream &os, const ZhangYulei &zhang_yulei)
{
    os << "************************" << std::endl
       << "Name of Student        : " << zhang_yulei.name_  << std::endl
       << "Title                  : " << "Genius Student" << std::endl
       << "Gender                 : " << ((zhang_yulei.gender_ == 0) ? "female" : "male") << std::endl
       << "Office                 : " << ((zhang_yulei.office_ == nullptr) ? "" : TString::Itoa(*zhang_yulei.office_, 10)) << std::endl
       << "Car number             : " << zhang_yulei.car_number_ << std::endl
       << "Friends of the zhang_yulei : ";
    for(auto friend_of_zhang_yulei_ = zhang_yulei.friends_of_student_.begin();
        friend_of_zhang_yulei_ != zhang_yulei.friends_of_student_.end();
        ++ friend_of_zhang_yulei_)
        os << (*friend_of_zhang_yulei_)->GetStudentName()
           << ((friend_of_zhang_yulei_ == zhang_yulei.friends_of_student_.end() - 1) ? "" : ", ");
    os << std::endl
       << "************************" << std::endl;

    return os;
}
