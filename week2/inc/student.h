#ifndef STUDENT_H
#define STUDENT_H

#include <vector>
#include <iostream>

#include "TString.h"

class Student
{
public:
    enum Gender{dFemale, dMale};

public:
    Student(TString name, Gender gender);
    Student(const Student &old_student);
    virtual ~Student();

    Student() = delete;
    Student(Student &&old_student) = delete;

    void SetStudentName(TString new_name) {name_ = new_name;}
    void SetStudentGender(Gender &new_gender) {gender_ = new_gender;}
    void SetStudentOffice(int new_office) {office_ = new int(new_office);}
    void AddStudentFriend(Student &new_friend);

    virtual TString GetStudentTitle() const {return "Common student";}
    TString GetStudentName() const {return name_;}
    TString GetStudentGender() const {return gender_;}
    int* GetStudentOffice() const {return office_;}
    std::vector<Student*> GetFriendsOfStudent() const {return friends_of_student_;}

    friend std::ostream& operator<<(std::ostream &os, const Student &student);

protected:
    void DeleteFriend(Student &old_friend);

    TString name_;
    Gender gender_;
    int *office_;

    std::vector<Student*> friends_of_student_;
};

#endif
